/*
* Created by jake on 6/20/14.
*/
import java.util.Scanner;
public class Killer {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		Killer kc = new Killer();
		while (true){
			System.out.print("Enter string:\t");
			String input = sc.nextLine();
			if (!input.equals("")){
				String revd = kc.preserveCase(input);
				revd = kc.reverse(revd);
				System.out.println(revd);
//				kc.isPalindrome(input, revd, true);
				if (kc.isPalindrome3(input)){
					System.out.println("Palindrome!");
				}
			}
			else{
				System.exit(0);
			}
		}


    }

	public String reverse(String input){ // reverses order of chars in a string
		String rev = "";
		for (int n = input.length() - 1; n >= 0; n--) {
			rev += input.charAt(n);

		}
		return rev;
	}


	public String preserveCase(String input){ //length of string - indexof(char) inherits case
		char[] fix = input.toCharArray();
		for (int i = 0; i < fix.length; i++){
			if (Character.isUpperCase(input.charAt(fix.length - 1 - i))){
				fix[i] = Character.toUpperCase(fix[i]);
			}
			else {
				fix[i] = Character.toLowerCase(fix[i]);
			}
		}
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < fix.length; i++){
			result.append(fix[i]);
		}
		return result.toString();

	}

//	public String undoCase(String input){ // switches case
//		String undone = "";
//		for (int n = 0; n < input.length(); n++){
//			if(Character.isUpperCase(input.charAt(n))){
//				undone += Character.toLowerCase(input.charAt(n));
//			}
//			else{
//				undone += Character.toUpperCase(input.charAt(n));
//			}
//		}
//		return undone;
//	}

	public boolean isPalindrome(String input, String reverse, boolean speak){
//		Killer kc = new Killer();
		if(input.toLowerCase().equals(reverse.toLowerCase())){
			if(speak) System.out.println("You entered a palindrome!");
			return true;
		}
		else return false;
	}

//	public boolean isPalindromeBest(String input){
//		if (input.equals("")) return true;
//		boolean pali = true;
//		input = input.toLowerCase().replace(" ", "");
//		int len = input.length();
//		if (input.length() % 2 == 0){ //even
//			for (int i = 0; i <= len / 2; i++){
//				int revInd = len - 1 - i;
//				if (input.charAt(i) != input.charAt(revInd)){
//					pali = false;
//					break;
//				}
//			}
//		}
//		else { //odd
//			String evenPali = input.substring(0, (len /2) ) + input.substring( (len /2) + 1 );
////			System.out.println(evenPali);
//			assert (evenPali.length() % 2 == 0);
//			return isPalindromeBest(evenPali); //recursion
//		}
//		return pali;
//	}

	public boolean isPalindrome3(String input){
		int k = input.length() - 1;

		if (k < 1)
			return true;


		if ( input.charAt(0) != input.charAt(k))
			return false;

		input = input.substring(1, k);

		//		System.out.println(input);

		return isPalindrome3(input);
	}
}
